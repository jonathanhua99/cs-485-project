﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gui_return : MonoBehaviour
{
    public GUISkin myskin;
    public int buttonWidth;
    public int buttonHeight;
    private int origin_x;
    private int origin_y;

    // Start is called before the first frame update
    void Start()
    {
        buttonWidth = 200;
        buttonHeight = 50;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void OnGUI()
    {
        GUI.skin = myskin;
        if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Return to Main Screen"))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}
