﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationOnce : MonoBehaviour
{
    private void FinishAnim()
    {
        Destroy(gameObject);
    }
}