﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    float moveSpeed = 7f;

    Rigidbody2D rb;

    PlayerController_v2 target;
    Vector2 moveDirection;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D> ();
		target = GameObject.FindObjectOfType<PlayerController_v2>();
		moveDirection = (target.transform.position - transform.position).normalized * moveSpeed;
		rb.velocity = new Vector2 (moveDirection.x, moveDirection.y);
		Destroy (gameObject, 3f);//bullet destroyed in 3 seconds if it doesn't collide with enemy
    
    }

    void OnTriggerEnter2D (Collider2D col)
	{
		if (col.gameObject.name.Equals ("Player")) {
			Debug.Log ("Hit!");
			Destroy (gameObject);
		}
	}
   
}
