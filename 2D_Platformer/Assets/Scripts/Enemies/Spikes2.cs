﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes2 : MonoBehaviour
{
    [SerializeField]
    private float
        lastTouchDamageTime,//last time spike touches enemy
        touchDamageCooldown,
        touchDamage,//amount of damage done
        touchDamageWidth,//bounding box area that damages player
        touchDamageHeight;//bounding box area that damages player

    [SerializeField]
    private Transform
    touchDamageCheck;//child of our enemy, center of touch damage area

    [SerializeField]
    private LayerMask
         whatIsPlayer;


    private Vector2
        touchDamageBotLeft,
        touchDamageTopRight;
    
    private float[] attackDetails = new float[2];
    
    private GameObject spikes;
    private Rigidbody2D spikesRb;
    private void Start()
    {
        spikes = transform.Find("spikes").gameObject;
        spikesRb = spikes.GetComponent<Rigidbody2D>();
        CheckTouchDamage();
    }

    private void CheckTouchDamage()
    {
        
        if (Time.time >= lastTouchDamageTime + touchDamageCooldown)
        {
            //determing areas
            touchDamageBotLeft.Set(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
            touchDamageTopRight.Set(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

            Collider2D hit = Physics2D.OverlapArea(touchDamageBotLeft, touchDamageTopRight, whatIsPlayer);

            if (hit != null)
            {
                lastTouchDamageTime = Time.time;
                attackDetails[0] = touchDamage;
                attackDetails[1] = spikes.transform.position.x;//this was part of applying knockback from the original script
                hit.SendMessage("Damage", attackDetails);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Vector2 botLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 botRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y - (touchDamageHeight / 2));
        Vector2 topRight = new Vector2(touchDamageCheck.position.x + (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));
        Vector2 topLeft = new Vector2(touchDamageCheck.position.x - (touchDamageWidth / 2), touchDamageCheck.position.y + (touchDamageHeight / 2));

        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(botRight, topRight);
        Gizmos.DrawLine(topRight, topLeft);
        Gizmos.DrawLine(topLeft, botLeft);

    }
}
