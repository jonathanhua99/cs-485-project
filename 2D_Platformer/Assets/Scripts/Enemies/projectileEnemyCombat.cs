﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class projectileEnemyCombat : MonoBehaviour
{
   [SerializeField]
	GameObject bullet;//creates bullet slot that needs bullet prefab

	float fireRate; //allows to fire bullets every 1 second
	float nextFire; //part of what allows to fire bullets every 1 second
   
    // Start is called before the first frame update
    void Start()
    {
        fireRate = 1.3f;//fire bullets every 1 second
		nextFire = Time.time;
    }

    // Update is called once per frame
    void Update()
    {
        CheckIfTimeToFire ();
    }

    void CheckIfTimeToFire()
	{
		if (Time.time > nextFire) {
			Instantiate (bullet, transform.position, Quaternion.identity);
			nextFire = Time.time + fireRate;
		}
		
	}

}
