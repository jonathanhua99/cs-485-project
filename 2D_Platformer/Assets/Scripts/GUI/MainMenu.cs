﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public int buttonWidth;
    public int buttonHeight;
    private int origin_x;
    private int origin_y;
    public GUISkin mySkin;
    

    // Start is called before the first frame update
    void Start()
    {
        buttonWidth = 200;
        buttonHeight = 50;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void OnGUI()
    {
        GUI.skin = mySkin;
        if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Level 1"))
        {
            globalVar.currentLevel = "Level One";
            SceneManager.LoadScene("instructionsScene");
        }
        if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight + 20, buttonWidth, buttonHeight), "Level 2"))
        {
            globalVar.currentLevel = "Level Two";
            SceneManager.LoadScene("Level Two");
        }
        if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 2 + 40, buttonWidth, buttonHeight), "Level 3"))
        {
            globalVar.currentLevel = "Level Three";
            SceneManager.LoadScene("Level Three"); 
        }
        if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight * 3 + 60, buttonWidth, buttonHeight), "Exit Game"))
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
    			Application.Quit();
#endif
        }
    }
}