﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class Trigger : MonoBehaviour
{
    private GameObject enemyExist;
    public UnityEngine.Events.UnityEvent GemToSpawn;
    private bool callOnce = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {     
        enemyExist = GameObject.FindGameObjectWithTag("Enemy");
        if (!callOnce)
        {
            if (enemyExist == null)
            {
                GemToSpawn.Invoke();
                callOnce = true;
            }
        }
    }
}
