﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathScript : MonoBehaviour
{
    public GUISkin myskin;
    public int buttonWidth;
    public int buttonHeight;
    private int origin_x;
    private int origin_y;

    // Start is called before the first frame update
    void Start()
    {
        buttonWidth = 200;
        buttonHeight = 50;
        origin_x = Screen.width / 2 - buttonWidth / 2;
        origin_y = Screen.height / 2 - buttonHeight * 2;
    }

    void OnGUI()
    {
        GUI.skin = myskin;
        if (GUI.Button(new Rect(origin_x, origin_y, buttonWidth, buttonHeight), "Continue?"))
        {
            SceneManager.LoadScene(globalVar.currentLevel);
        }
        if (GUI.Button(new Rect(origin_x, origin_y + buttonHeight + 20, buttonWidth, buttonHeight), "Main Menu"))
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
}