﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move_background : MonoBehaviour
{
    private Vector3 direction = new Vector3(1, 0, 0);
    private Vector3 angle = new Vector3(1,1,1);
    void Start()
    {

    }

    private void Update()
    {
        transform.Translate(direction * Time.deltaTime, Space.World);
        if (this.gameObject.transform.position.x > 20)
        {
            this.gameObject.transform.position = new Vector3 (0,0,-10);
        }
    }
}
