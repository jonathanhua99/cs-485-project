﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraFollow : MonoBehaviour
{
    private Transform playerTransform;
    public float offsetX;//offset to adjust X axis of camera on player
    public float offsetY;//offset to adjust Y axis of camera on player 

    // Start is called before the first frame update
    void Start()
    {
        playerTransform = GameObject.FindGameObjectWithTag("Player").transform;
    }

    
    // Update is called once per frame
    void Update()
    { }//nothing needed, keeping just to explain what LateUpdate means

    //called every fixed frame rate
    void FixedUpdate()
    {}//nothing needed

    //called after update and fixed update
    void LateUpdate()
    {
        //we store current camera's position in variable temp - temporary position
        Vector3 temp = transform.position;

        //we set camera's position x to be equal to the player's position x
        temp.x = playerTransform.position.x;
        //we set camera's position y to be equal to the player's position y
        temp.y = playerTransform.position.y;//comment out this line to prevent moving the camera vertical

        //this will add the offset value to the temporary camera x position
        temp.x += offsetX;
        //this will add the offset value to the temporary camera y position
        temp.y += offsetY;

        //we set the camera's temp position to the camera's current position
        transform.position = temp;
    }
}
