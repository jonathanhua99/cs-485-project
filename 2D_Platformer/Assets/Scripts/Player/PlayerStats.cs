﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerStats : MonoBehaviour
{
    [SerializeField]
    private float maxHealth;

    private float currentHealth;
    public static string levelName;

    //private GameManager GM;

    private void Start()
    {
        DontDestroyOnLoad(transform.gameObject);
        levelName = SceneManager.GetActiveScene().name;
        currentHealth = maxHealth;
    }

    public void DecreaseHealth(float amount)
    {
        currentHealth -= amount;

        if (currentHealth <= 0.0f)
        {
            Die();
        }
    }
    IEnumerator Pause()
    {
        yield return new WaitForSeconds(2);
    }

    private void Die()
    {
        Destroy(gameObject);
        Pause();
        SceneManager.LoadScene("DeathScreen");
    }
}
