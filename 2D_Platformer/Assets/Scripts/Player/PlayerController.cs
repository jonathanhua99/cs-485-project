﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour 
{
    public Vector3 velocity;
	Rigidbody2D rb;
	private Animator anim;
	float moveSpeed;
	private float dirX;
	//int  healthPoints = 2;//I'll figure this out later
	//bool isDead;
	bool facingRight = true;
	private Vector3 localScale;
	private int facingDirection = 1;

	public Transform groundCheck;
	private bool isGrounded;
	public float groundCheckRadius;
	public LayerMask whatIsGround;

	//for dashing
	[Header("Dashing")]
	public bool canDash = true;
	public float dashingTime;
	public float dashSpeed;
	public float dashJumpIncrease;
	public float timeBtwDashes;//so you can't spam dashes
	public bool canMove = true;//
	public float jumpForce = 16f;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		anim = GetComponent<Animator> ();
		localScale = transform.localScale;
		moveSpeed = 5f;
	}
	
	// Update is called once per frame
	void Update () {	

		dirX = Input.GetAxisRaw ("Horizontal") * moveSpeed;

		if (Input.GetButtonDown ("Jump") && rb.velocity.y == 0)
			rb.AddForce (Vector2.up * 700f);

		if(Mathf.Abs(dirX) > 0 && rb.velocity.y == 0)
			anim.SetBool("isRunning", true);
		else
			anim.SetBool("isRunning", false);

		if(rb.velocity.y == 0)
		{
			anim.SetBool("isJumping", false);
			anim.SetBool("isFalling", false);
		}

		if(rb.velocity.y > 0)
			anim.SetBool("isJumping", true);

		if(rb.velocity.y < 0)
		{
			anim.SetBool("isJumping", false);
			anim.SetBool("isFalling", true);
		}
	}

	private void FixedUpdate()
	{
		rb.velocity = new Vector2(dirX, rb.velocity.y);
		if(canMove)
		{
			if(Input.GetKeyDown(KeyCode.F))//LeftShift
			{
				DashAbility();
			}
		}
	}

	private void LateUpdate()
	{
		if(dirX > 0)
			facingRight = true;
		else if(dirX < 0)
			facingRight = false;
		
		if(((facingRight) && (localScale.x < 0)) || ((!facingRight) && (localScale.x > 0)))
			localScale.x *= -1;

		transform.localScale = localScale;
	}

	private void CheckSurroundings()
	{
		isGrounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
	}

	private void DashAbility()
	{
		if(canDash)
		{
			StartCoroutine(Dash());
		}
	}

	IEnumerator Dash()
	{
		canDash = false;
		moveSpeed = dashSpeed;
		jumpForce = dashJumpIncrease;
		yield return new WaitForSeconds(dashingTime);
		moveSpeed = 5f;//or w/e the default moveSpeed is
		jumpForce = 18.5f;//higher than the default (from above) so player gets a higher jump when dashing (hopefully lol)
		yield return new WaitForSeconds(timeBtwDashes);
		canDash = true;
	}

	public int GetFacingDirection()
	{
		return facingDirection;
	}

	private void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere(groundCheck.position, groundCheckRadius);
	}

}	//end

